#!/bin/sh

if [ "$TERM" = unknown ] || [ -z "$TERM" ]; then
  export TERM=xterm
fi

LOCATION="/usr/share/liquidprompt/liquidprompt"

set -e
zsh -ci "source $LOCATION"
